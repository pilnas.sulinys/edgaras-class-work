from enum import Enum

from pydantic import BaseModel, Field, PositiveInt
from datetime import datetime

class Book(BaseModel):
    title: str
    publisched_date: datetime.time
    house_id: int


class ReplaceBook(BaseModel):
    title: str
    publisched_date: datetime.time
    house_id: int


class UpdatePost(BaseModel):
    title: str | None
    publisched_date: datetime.time | None
    house_id: int | None
