from db_session import postgres, pg_errors
from fastapi import Body, FastAPI, HTTPException, Path, Response, status
from book import Book, ReplaceBook, UpdateBook
from pydantic import PositiveInt

###### FastAPI instance name ######
app = FastAPI()


def find_book(id: PositiveInt) -> Book:
    """Find book in database base on id

    Args:
        id (PositiveInt): id of the post

    Raises:
        HTTPException: if id is not found in db raise exception

    Returns:
        Post: Return RealDictRow (ordered dict subclass)
    """

    with postgres.conn:
        with postgres.cursor() as curs:
            curs.execute("select * from book")
            books = curs.fetchall()

    for book in books:
        for key, value in book.items():
            if key == "id" and value == id:
                return book
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND, detail=f"No book was found with id {id}"
    )


# GET /posts ***GET ALL BLOGPOSTS***
@app.get("/books")
def get_posts():
    with postgres.conn:
        with postgres.cursor() as curs:
            curs.execute("select * from book")
            database_posts = curs.fetchall()
    return {"data": database_posts}


# GET /posts/{id_param}  ***GET POST WITH ID**
@app.get("/books/{id_param}")
def get_posts(id_param: PositiveInt):
    return {"data": find_book(id_param)}


# POST /posts  ***CREATE NEW BLOGPOST***
@app.post("/books", status_code=status.HTTP_201_CREATED)
def create_post(response: Response, book: Book = Body(default=True)):
    with postgres.conn:
        with postgres.cursor() as curs:
            try:
                curs.execute(
                    "insert into book (title, publisched_date, house_id) values (%(title)s, %(publisched_date)s, %(house_id)s) RETURNING *;",
                    {
                        "title": book.title,
                        "publisched_date": book.publisched_date,
                        "house_id": book.house_id,
                    },
                )
            except pg_errors.ForeignKeyViolation:
                raise HTTPException(
                    status.HTTP_500_INTERNAL_SERVER_ERROR,
                    detail=f"Foreign key violation with user id {book.house_id}",
                )
            database_post = curs.fetchone()
    return dict(data=database_post, query=curs.query)


# DELETE /posts/{id_param}  ***DELETE POST WITH ID***
@app.delete("/books/{id_param}", status_code=status.HTTP_200_OK)
def delete_post(id_param: PositiveInt, response: Response):
    book = find_book(id_param)
    with postgres.conn:
        with postgres.cursor() as curs:
            curs.execute(
                "delete from book where id = %(id)s RETURNING *;",
                {"id": book["id"]},
            )
            database_post = curs.fetchone()
    return dict(data=database_post)


# PUT /posts/{id_param}  ***REPLACE POST WITH ID***
@app.put("/books/{id_param}", status_code=200)
def replace_post(id_param: PositiveInt, renewed_book: ReplaceBook, response: Response):
    book = find_book(id_param)
    with postgres.conn:
        with postgres.cursor() as curs:
            try:
                updated_data = {
                    **book,
                    **renewed_book.dict(),
                }
                curs.execute(
                    "update book set title=%(title)s, publisched_date=%(publisched_date)s, house_id=%(house_id)s where id=%(id)s RETURNING *;",
                    updated_data,
                )
            except pg_errors.ForeignKeyViolation:
                raise HTTPException(
                    status.HTTP_500_INTERNAL_SERVER_ERROR,
                    detail=f"Foreign key violation with user id {updated_data['house_id']}",
                )
            database_post = curs.fetchone()
    return dict(data=database_post)


# PATCH /posts/{id_param}  ***UPDATE POST WITH ID***
@app.patch("/posts/{id_param}", status_code=200)
def update_post(id_param: PositiveInt, response: Response, updated_book: UpdateBook):
    original_book = find_book(id_param)
    with postgres.conn:
        with postgres.cursor() as curs:
            try:
                updated_data = {
                    **original_book,
                    **updated_book.dict(exclude_unset=True),
                }
                curs.execute(
                    "update book set title=%(title)s, publisched_date=%(publisched_date)s, house_id=%(house_id)s where id=%(id)s RETURNING *;",
                    updated_data,
                )
            except pg_errors.ForeignKeyViolation:
                raise HTTPException(
                    status.HTTP_500_INTERNAL_SERVER_ERROR,
                    detail=f"Foreign ke violation with user id {updated_data['house_id']}",
                )
            database_post = curs.fetchone()

    return dict(data=database_post)
