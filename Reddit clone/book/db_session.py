import psycopg2
from psycopg2.extras import RealDictCursor

# better get from system env variables or from encrypted filess
db_connection_settings = {
    "host" : "localhost",
    "dbname" : "onetomany", 
    "user" : "postgres",
    "password" : "admin",
    "cursor_factory": RealDictCursor
}

pg_errors = psycopg2.errors

class SingletonMeta(type):
    """
    The Singleton class can be implemented in different ways in Python. Some
    possible methods include: base class, decorator, metaclass. We will use the
    metaclass because it is best suited for this purpose.
    """

    _instances = {}

    def __call__(cls, *args, **kwargs):
        """
        Possible changes to the value of the `__init__` argument do not affect
        the returned instance.
        """
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class Connection(metaclass=SingletonMeta):
    def __init__(self, *args, **kwargs):
        
        self.db_version = None
        self.conn = psycopg2.connect(**db_connection_settings)
        self.cursor = self.conn.cursor
        
        with self.cursor() as cursor:
            cursor.execute('SELECT VERSION()')
            self.db_version = cursor.fetchone()
    
    def __del__(self):
        self.conn.close()
        self.cursor.close()



try:
    postgres = Connection()     
except psycopg2.DatabaseError as error:
    print(f"Database connection error: {error}")


