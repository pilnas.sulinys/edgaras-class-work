from enum import Enum

from pydantic import BaseModel, Field, PositiveInt


class Highlight(int, Enum):
    yes = 1


# class IdGenerator:
#     id: int = 0

#     @classmethod
#     def get_id(cls):
#         cls.id += 1
#         return cls.id


class Post(BaseModel):
    # id: PositiveInt = Field(default_factory=IdGenerator.get_id)
    # author: str
    content: str
    title: str
    published: bool = False
    # rating: int | None = None
    # highlight: Highlight | None = None
    writer_id: int

    # def __hash__(self) -> int:
    #     return self.id

    # def __eq__(self, other) -> bool:
    #     if type(self) is type(other):
    #         return self.id == other.id
    #     return False


class ReplacePost(BaseModel):
    # author: str
    content: str
    title: str
    published: bool = True
    writer_id: int
    # rating: int | None = None
    # highlight: Highlight | None = None


class UpdatePost(BaseModel):
    # author: str | None
    content: str | None
    title: str | None
    published: bool = True
    writer_id: int | None
    # rating: int | None = None
    # highlight: Highlight | None = None
