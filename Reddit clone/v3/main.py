from db_session import postgres, pg_errors
from fastapi import Body, FastAPI, HTTPException, Path, Response, status
from posts import Post, ReplacePost, UpdatePost
from pydantic import PositiveInt

###### FastAPI instance name ######
app = FastAPI()


def find_post(id: PositiveInt) -> Post:
    """Find post in database base on id

    Args:
        id (PositiveInt): id of the post

    Raises:
        HTTPException: if id is not found in db raise exception

    Returns:
        Post: Return RealDictRow (ordered dict subclass)
    """

    with postgres.conn:
        with postgres.cursor() as curs:
            curs.execute("select * from blogpost")
            database_posts = curs.fetchall()

    for post in database_posts:
        for key, value in post.items():
            if key == "id" and value == id:
                return post
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND, detail=f"No post was found with id {id}"
    )


# GET /posts ***GET ALL BLOGPOSTS***
@app.get("/posts")
def get_posts():
    with postgres.conn:
        with postgres.cursor() as curs:
            curs.execute("select * from blogpost")
            database_posts = curs.fetchall()
    return {"data": database_posts}


# GET /posts/{id_param}  ***GET POST WITH ID**
@app.get("/posts/{id_param}")
def get_posts(id_param: PositiveInt):
    return {"data": find_post(id_param)}


# POST /posts  ***CREATE NEW BLOGPOST***
@app.post("/posts", status_code=status.HTTP_201_CREATED)
def create_post(response: Response, post: Post = Body(default=True)):
    with postgres.conn:
        with postgres.cursor() as curs:
            try:
                curs.execute(
                    "insert into blogpost (title, content, writer_id) values (%(title)s, %(content)s, %(writer_id)s) RETURNING *;",
                    {
                        "title": post.title,
                        "content": post.content,
                        "writer_id": post.writer_id,
                    },
                )
            except pg_errors.ForeignKeyViolation:
                raise HTTPException(
                    status.HTTP_500_INTERNAL_SERVER_ERROR,
                    detail=f"Foreign ke violation with user id {post.writer_id}",
                )
            database_post = curs.fetchone()
    return dict(data=database_post, query=curs.query)


# DELETE /posts/{id_param}  ***DELETE POST WITH ID***
@app.delete("/posts/{id_param}", status_code=status.HTTP_200_OK)
def delete_post(id_param: PositiveInt, response: Response):
    post = find_post(id_param)
    with postgres.conn:
        with postgres.cursor() as curs:
            curs.execute(
                "delete from blogpost where id = %(id)s RETURNING *;",
                {"id": post["id"]},
            )
            database_post = curs.fetchone()
    return dict(data=database_post)


# PUT /posts/{id_param}  ***REPLACE POST WITH ID***
@app.put("/posts/{id_param}", status_code=200)
def replace_post(id_param: PositiveInt, renewed_post: ReplacePost, response: Response):
    print(renewed_post)
    post = find_post(id_param)
    with postgres.conn:
        with postgres.cursor() as curs:
            try:
                updated_data = {
                    **post,
                    **renewed_post.dict(),
                }
                curs.execute(
                    "update blogpost set title=%(title)s, content=%(content)s, published=%(published)s, writer_id=%(writer_id)s where id=%(id)s RETURNING *;",
                    updated_data,
                )
            except pg_errors.ForeignKeyViolation:
                raise HTTPException(
                    status.HTTP_500_INTERNAL_SERVER_ERROR,
                    detail=f"Foreign ke violation with user id {updated_data['writer_id']}",
                )
            database_post = curs.fetchone()
    return dict(data=database_post)


# PATCH /posts/{id_param}  ***UPDATE POST WITH ID***
@app.patch("/posts/{id_param}", status_code=200)
def update_post(id_param: PositiveInt, response: Response, updated_post: UpdatePost):
    original_post = find_post(id_param)
    with postgres.conn:
        with postgres.cursor() as curs:
            try:
                updated_data = {
                    **original_post,
                    **updated_post.dict(exclude_unset=True),
                }
                curs.execute(
                    "update blogpost set title=%(title)s, content=%(content)s, published=%(published)s, writer_id=%(writer_id)s where id=%(id)s RETURNING *;",
                    updated_data,
                )
            except pg_errors.ForeignKeyViolation:
                raise HTTPException(
                    status.HTTP_500_INTERNAL_SERVER_ERROR,
                    detail=f"Foreign ke violation with user id {updated_data['writer_id']}",
                )
            database_post = curs.fetchone()

    return dict(data=database_post)
