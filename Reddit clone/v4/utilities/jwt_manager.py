from jose import jwt, JWTError
from .schemas import Token
from fastapi import HTTPException, status, Depends
from fastapi.security import OAuth2PasswordBearer
from ..config import settings

SERVER_KEY = settings.secret_key
ALHORITHM = settings.algorithm

oauth2_schema = OAuth2PasswordBearer(tokenUrl="/auth")

# encode token
def generate_token(user_id: int):

    payload = {"user_id": user_id}
    encoded_jwt = jwt.encode(payload, SERVER_KEY, ALHORITHM)
    return Token(access_token=encoded_jwt, token_type="bearer")


# depreciated: decode token
def _decoded_token(provided_token: str):
    try:
        payload = jwt.decode(provided_token, SERVER_KEY, algorithms=[ALHORITHM])
        decoded_id = payload.get("user_id")
    except JWTError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return decoded_id


# decode token
def decoded_token(provided_token: str = Depends(oauth2_schema)):
    try:
        payload = jwt.decode(provided_token, SERVER_KEY, algorithms=[ALHORITHM])
        decoded_id = payload.get("user_id")
    except JWTError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return decoded_id
