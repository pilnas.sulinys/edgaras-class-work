from pydantic import BaseModel, EmailStr

class Token(BaseModel):
    access_token: str
    token_type: str = "Bearer"


class User_Credentials(BaseModel):
    email: EmailStr
    password: str
