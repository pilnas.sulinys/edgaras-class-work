"""Receive and hande the HTTP requests
"""

from .database import get_db, database_engine
from fastapi import Depends, FastAPI, HTTPException, Response, status
from sqlalchemy.orm import Session
from . import models, schemas
from .utilities import hash_manager, jwt_manager
from .utilities.schemas import Token, User_Credentials
from .errors import IncorrectInfoException
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from fastapi.middleware.cors import CORSMiddleware


models.Base.metadata.create_all(bind=database_engine)

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# auth endpoint
@app.post("/auth", response_model=Token)
def auth_user(
    user_credentials: OAuth2PasswordRequestForm = Depends(),
    db: Session = Depends(get_db),
):
    corresponding_user: models.User = (
        db.query(models.User)
        .filter(models.User.email == user_credentials.username)
        .first()
    )

    if not corresponding_user:
        raise IncorrectInfoException

    pass_valid = hash_manager.verify_password(
        user_credentials.password, corresponding_user.password
    )
    if not pass_valid:
        raise IncorrectInfoException

    return jwt_manager.generate_token(corresponding_user.id)


#

# Users
@app.get("/users", response_model=list[schemas.User_Response])
def get_users(
    token: str = Depends(jwt_manager.oauth2_schema), db: Session = Depends(get_db)
):
    all_users = db.query(models.User).all()
    return all_users


@app.get("/users/me", response_model=schemas.User_Response)
def get_me(
    user_id: int = Depends(jwt_manager.decoded_token), db: Session = Depends(get_db)
):
    corresponding_user = db.query(models.User).filter(models.User.id == user_id).first()
    return corresponding_user


@app.get("/users/{user_id}", response_model=schemas.User_Response)
def get_user(user_id: int, db: Session = Depends(get_db)):
    user = db.query(models.User).filter(models.User.id == user_id).first()
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"No user with id {user_id} was found",
        )
    return user


@app.post("/users", response_model=schemas.User_Response)
def post_user(
    user_body: schemas.UserPy,
    db: Session = Depends(get_db),
):
    pwd_hashed = hash_manager.hash_pass(user_body.password)
    user_body.__dict__.update({"password": pwd_hashed})
    new_user = models.User(**user_body.dict())
    db.add(new_user)
    db.commit()
    return new_user


# posts

# get specific
@app.get("/posts", response_model=list[schemas.BlogPostResponse])
def get_posts(
    db: Session = Depends(get_db),
    current_user: int = Depends(jwt_manager.decoded_token),
):
    all_posts = db.query(models.BlogPost).all()
    return all_posts


# get specific post
@app.get("/posts/{post_id}", response_model=schemas.BlogPostResponse)
def get_post(
    post_id: int,
    current_user: int = Depends(jwt_manager.decoded_token),
    db: Session = Depends(get_db),
):
    post = db.query(models.BlogPost).filter(models.BlogPost.id == post_id).first()
    if not post:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"No post with id {post_id} was found",
        )
    return post


@app.put("/posts/{post_id}", response_model=schemas.BlogPostResponse)
def update_post(
    post_id: int,
    user_body: schemas.BlogPostPy,
    db: Session = Depends(get_db),
    current_user: int = Depends(jwt_manager.decoded_token),
):
    post = db.query(models.BlogPost).filter(models.BlogPost.id == post_id)
    if not post:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"No post with id {post_id} was found",
        )
    if post.first().writer_id != current_user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=f"You are not the author of the post {post_id}",
        )

    post.update(user_body.dict())
    db.commit()

    return post.first()
