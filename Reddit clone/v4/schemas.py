"""pydantic schemas to exchange data structures beetween API server and client consumer
"""

from datetime import datetime

from pydantic import BaseModel, EmailStr


class UserPy(BaseModel):
    email: EmailStr
    password: str


class User_Response(BaseModel):
    id: int
    created_at: datetime
    email: EmailStr

    class Config:
        orm_mode = True


class BlogPostPy(BaseModel):
    title: str
    content: str
    # writer_id: int
    published: bool = True


class BlogPostResponse(BlogPostPy):
    id: int
    created_at: datetime
    writer: User_Response

    class Config:
        orm_mode = True
