from typing import Any

from fastapi import HTTPException, status


class IncorrectInfoException(HTTPException):
    def __init__(
        self
    ) -> None:

        status_code = status.HTTP_401_UNAUTHORIZED
        detail = "Could not validate credentials"
        headers = {"WWW-Authenticate": "Bearer"}

        super().__init__(status_code=status_code, detail=detail, headers=headers)
