"""Create and close database sessions
"""
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from .config import settings

db_connection_settings = {
    "host": settings.database_host,
    "dbname": settings.database_name,
    "user": settings.database_username,
    "password": settings.database_password,
    "port": settings.dtabase_port,
}

DATABASE_URL = (
    f"postgresql://{db_connection_settings['user']}:{db_connection_settings['password']}"
    f"@{db_connection_settings['host']}:{db_connection_settings['port']}/{db_connection_settings['dbname']}"
)

database_engine = create_engine(DATABASE_URL)
SessionTemplate = sessionmaker(autocommit=False, autoflush=False, bind=database_engine)


def get_db():
    db = SessionTemplate()
    try:
        yield db
    finally:
        db.close()
