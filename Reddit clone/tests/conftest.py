import pytest
from fastapi.testclient import TestClient
from v4.main import app
from v4.models import Base
from typing import Generator
from v4.database import *
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

db_connection_settings = {
    "host": "localhost",
    "dbname": "sqlalchemy_test",
    "user": "postgres",
    "password": "admin",
}

DATABASE_URL = (
    f"postgresql://{db_connection_settings['user']}:{db_connection_settings['password']}"
    f"@{db_connection_settings['host']}:5432/{db_connection_settings['dbname']}"
)

database_engine = create_engine(DATABASE_URL)
TestingSessionTemplate = sessionmaker(
    autocommit=False, autoflush=False, bind=database_engine
)


def testing_get_db():
    db = TestingSessionTemplate()
    try:
        yield db
    finally:
        db.close()


@pytest.fixture
def session():
    Base.metadata.drop_all(bind=database_engine)
    Base.metadata.create_all(bind=database_engine)
    app.dependency_overrides[get_db] = testing_get_db


@pytest.fixture
def client(session) -> TestClient:
    yield TestClient(app)


@pytest.fixture
def user_credentials():
    yield dict(email="test@gmail.com", password="1234")


@pytest.fixture
def create_user(client: TestClient, user_credentials):
    res = client.post(
        "/users",
        json=user_credentials,
    )
    yield {**res.json(), "password": user_credentials["password"]}


@pytest.fixture
def user_token(create_user, client: TestClient):
    res = client.post(
        "/auth/",
        data={"username": create_user["email"], "password": create_user["password"]},
    )
    return res.json().get("access_token")


@pytest.fixture
def authorized_client(client: TestClient, user_token):
    client.headers = {**client.headers, "Authorization": f"Bearer {user_token}"}
    return client
