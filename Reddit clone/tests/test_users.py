from v4.utilities.jwt_manager import decoded_token, SERVER_KEY, ALHORITHM
from jose import jwt
import pytest


def test_create_user(client) -> None:
    res = client.post(
        "/users",
        json=dict(email="test@gmail.com", password="1234"),
    )
    assert res.json().get("email") == "test@gmail.com"
    assert res.status_code == 200


from enum import IntEnum


class RequestStatus(IntEnum):
    MISSING_PASSWORD = 422
    MISSING_EMAIL = 422
    WRONG_EMAIL = 401
    WRONG_PASSWORD = 401
    WRONG_EMAIL_PASSWORD = 401


login = [
    pytest.param(
        {"username": "test"}, RequestStatus.MISSING_PASSWORD, id="Missing password"
    ),
    pytest.param({"password": "test"}, RequestStatus.MISSING_EMAIL, id="Missing email"),
    pytest.param(
        {"username": "testt@gmail.com", "password": "1234"},
        RequestStatus.WRONG_EMAIL,
        id="Wrong email",
    ),
    pytest.param(
        {"username": "test@gmail.com", "password": "12324"},
        RequestStatus.WRONG_PASSWORD,
        id="Wrong password",
    ),
    pytest.param(
        {"username": "testt@gmail.com", "password": "testt"},
        RequestStatus.WRONG_EMAIL_PASSWORD,
        id="Wrong username and password",
    ),
]


@pytest.mark.parametrize("data,status", login)
def test_login_failure(client, data, status, create_user):
    rest = client.post(
        "/auth",
        data=data,
    )
    assert status == rest.status_code


def test_login_user(create_user, client):
    rest = client.post(
        "/auth",
        data={"username": create_user["email"], "password": create_user["password"]},
    )
    assert 200 == rest.status_code
    assert rest.json().get("token_type") == "bearer"
    payload = jwt.decode(
        rest.json().get("access_token"), SERVER_KEY, algorithms=[ALHORITHM]
    )
    assert payload == {"user_id": create_user["id"]}


def test_me(create_user, authorized_client):
    res = authorized_client.get("/users/me/")
    assert res.status_code == 200
    assert res.json()["id"] == create_user["id"]
    assert res.json()["email"] == create_user["email"]
