import pytest


def test_101():
    print("Test whatever you want")
    assert 100 + 1 == 101


# def total_with_tip(bill, percentage):
#     return bill + bill * percentage / 100


def test_tip_100():
    print("tipping 2% on 100$")
    assert total_with_tip(100, 2) == 102


def total_with_tip(bill, percentage):
    if bill < 0 or percentage < 0:
        raise Exception("Bill and Percentage have to tbe positve")
    return bill + bill * percentage / 100


@pytest.mark.parametrize(
    "num1, num2, expectation", [(10, 20, 12), (100, 20, 120), (0, 0, 0)]
)
def test_tip_bul(num1, num2, expectation):
    assert total_with_tip(num1, num2) == expectation
