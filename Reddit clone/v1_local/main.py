from fastapi import Body, FastAPI, HTTPException, Path, Response, status
from pydantic import PositiveInt

from posts import Post, ReplacePost, UpdatePost

app = FastAPI()

posts = [
    Post(
        author="Edgaras",
        content="I like this class",
        title="Python API",
        published=True,
    ),
    Post(author="Jonas", content="I like this class too", title="Python API"),
    Post(author="Reuben", content="Python is my favorite language", title="Python API"),
]


def find_post(id: PositiveInt) -> Post:
    for post in posts:
        if post.id == id:
            return post
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND, detail=f"No post was found with id {id}"
    )


@app.get("/posts")
def get_posts():
    return {"data": posts}


@app.get("/posts/{id_param}")
def get_posts(id_param: PositiveInt):
    return {"data": find_post(id_param)}


@app.post("/posts", status_code=201)
def create_post(response: Response, post: Post = Body(default=True)):
    posts.append(post)
    return dict(data=post)


@app.delete("/posts/{id_param}", status_code=200)
def delete_post(id_param: PositiveInt, response: Response):
    post = find_post(id_param)
    posts.remove(post)
    return dict(data=post)


def replace_that_post(original_post: Post, renewed_post: ReplacePost) -> Post:
    # shorter is this one:
    # original_post.__dict__.update(**dict(renewed_post))
    
    posts.remove(original_post)
    replaced_post = original_post.copy(update=renewed_post.dict())
    posts.append(replaced_post)
    
    return replaced_post


def update_that_post(original_post: Post, updated_post: UpdatePost) -> None:
    # maybe better as with replace_that_post
    original_post.__dict__.update(**updated_post.dict(exclude_unset=True))


@app.put("/posts/{id_param}", status_code=200)
def replace_post(id_param: PositiveInt, renewed_post: ReplacePost, response: Response):
    original_post = find_post(id_param)
    replaced_post = replace_that_post(original_post, renewed_post)
    return dict(data=replaced_post)


@app.patch("/posts/{id_param}", status_code=200, response_model=Post, response_model_exclude_unset=False)
def update_post(id_param: PositiveInt, response: Response, updated_post: UpdatePost):
    original_post = find_post(id_param)
    update_that_post(original_post, updated_post)
    return original_post
