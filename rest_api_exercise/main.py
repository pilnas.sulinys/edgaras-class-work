from account_holder import AccountHolder
import requests


class Bank:
    def __init__(self, name: str) -> None:
        self.name = name
        self._clients: list[AccountHolder] = []
        self._api_endpoint = (
            "https://637cca3816c1b892ebbf2fd9.mockapi.io/api/v1/clients"
        )

        self._read_clients()

    def _read_clients(self) -> None:
        req = requests.get(self._api_endpoint)
        clients = req.json()

        for client in clients:
            match client:
                case {
                    "name": name,
                    "account": account,
                    "amount": amount,
                    "id": id,
                    "createdAt": created,
                }:
                   
                    account_holder = AccountHolder(
                        **{
                            "name": name,
                            "account": account,
                            "amount": amount,
                            "id": int(id),
                        }
                    )
                    self._clients.append(account_holder)
                case _:
                    raise ValueError("API changed, can't understand the data")

    def remove_client(self, name: str, account: str) -> None:
        id = self._get_id(name, account)
        req = requests.delete(self._api_endpoint+f"/{id}")
        if req.ok:
            print(f"Client {name} with account {account} removed: {req.content}")
        else:
            print("Client {name} with account {account} was not removed: {req.status_code} {req.content}")
    
    def _get_id(self, name:str, account: str) -> int:
        for client in self._clients:
            if client.name == name and client.account == account:
                return client.id
        raise ValueError(f"Client with name {name} and account {account} was not found.")

    def add_client(self, name: str, account: str, amount: float) -> None:
        for client in self._clients:
            if client.name == name and client.account == account:
                raise ValueError("Client {name} with account {account} already exists!")
        req = requests.post(self._api_endpoint, data=dict(name=name, account=account, amount=amount))
        if req.ok:
            print(f"Client {name} with account {account} created: {req.content}")
        else:
            print(f"Client {name} with account {account} was not created: {req.status_code} {req.content}")
            

    def change_balance(self, name: str, account: str, amount: float) -> None:
        for client in self._clients:
            if client.name == name and client.account == account:
                requests.put(
                    self._api_endpoint + f"/{client.id}",
                    data={"amount": client.amount + amount},
                )
                client.amount = client.amount + amount

    def get_balance(self, name: str, account: str) -> None:
        for client in self._clients:
            if client.name == name and client.account == account:
                return client.amount


ing_bank = Bank("ING")
print(ing_bank.change_balance("Edgaras", "LT100010", -100))
print(ing_bank.get_balance("Edgaras", "LT100010"))

