from pydantic import BaseModel, PositiveInt
import json


class AccountHolderEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, AccountHolder):
            return dict(name=obj.name, account=obj.account, money=obj.money, id=obj.id)
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)


class AccountHolder(BaseModel):
    name: str
    account: str
    amount: float
    id: PositiveInt

    def __str__(self):
        return json.dumps(dict(self), ensure_ascii=False, cls=AccountHolder)

    def to_json(self):
        return self.__str__()
