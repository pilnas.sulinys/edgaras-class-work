class Calculation:

    def factorial(self, number: int) -> int:
        if number < 0:
            raise ValueError("Can't calculate factorial for negative numbers")
        if number == 0:
            return 1
        return number * self.factorial(number - 1)

    def sum(self, number: int) -> int:
        if number < 0:
            raise ValueError("Can't calculate cumulative sum for negative numbers")

        match number:
            case 0:
                return 0
            case _:
                return number + self.sum(number - 1)

    def prime(self, number: int) -> bool:
        if number < 0:
            raise ValueError("Can't test if prime sum negative numbers")

        if number != 2 and number % 2 == False:
            return False

        for divisor in range(3, int(number ** (1 / 2)) + 1, 2):
            if number % divisor == 0:
                return False
        return True

    def twinprimes(self, number: int) -> bool:
        if number < 0:
            raise ValueError("Can't do twin prime test for negative integer")

        if self.prime(number) and number > 2 and self.prime(number - 2):
            return True
        if self.prime(number) and self.prime(number + 2):
            return True

        return False

    def multiplitable(self, number: int) -> str:
        if number > 10 or number < 1:
            raise ValueError(
                "Can't show multiplication table for numbers outside interval [1, 10]"
            )

        table = ""
        for i in range(1, 11):
            table = table + f"{number:<2}*{i:<2} == {number*i:>2} {', ' if i < 10 else ' '} "
        table = table + "\n"

        return table

    def tenmultiplitables(self) -> str:
        multi_table = ""
        for i in range(1, 11):
            multi_table += self.multiplitable(i)
        return multi_table

    def divlist(self, number: int) -> list[int]:
        if number < 0:
            raise ValueError("Can't find divisors for negative integer")
        divisors = []
        for i in range(1, number // 2 + 1):
            if number % i == 0:
                divisors.append(i)
        divisors.append(number)

        return divisors
    
    def primedivlist(self, number: int) -> list[int]:
        """
        >>> c.primedivlist(10)
        [1, 2, 5]
        """
        if number < 0:
            raise ValueError("Can't find prime divisors for negative integer")
        divisors = self.divlist(number)
        prime_divisors = [divisor for divisor in divisors if self.prime(divisor)]
        return prime_divisors


if __name__ == "__main__":
    # import doctest

    # doctest.testmod(extraglobs={'c': Calculation()}, verbose=True)

    calc = Calculation()
    print(f"Factorial of 5: {calc.factorial(5)}")
    print(f"Cumulative sum of 10: {calc.sum(10)}")
    print(f"Number 14 is prime: {calc.prime(14)}")
    print(f"Number 19 is prime: {calc.prime(19)}")

    print(f"Number 19 is twin prime: {calc.twinprimes(19)}")
    print(f"Number 29 is twin prime: {calc.twinprimes(29)}")
    print(f"Number 23 is twin prime: {calc.twinprimes(23)}")

    print(f"Multiplitable for 7: {calc.multiplitable(7)}")
    print(f"Ten multiplitables: \n{calc.tenmultiplitables()}")

    print(f"Divisors of 255: {calc.divlist(255)}")
    print(f"Prime divisors of 255: {calc.primedivlist(255)}")
