class CaesarEncryption:
    min_printable_char = 32
    max_pritable_char = 126

    def __init__(self, text: str = "", key_text: str = ""):
        self.text = text
        self.encrypted_msg = ""
        self.key_text = key_text

    def encryption(self, *, text=None, key_text=None, decryption=False) -> None:

        if text is not None:
            self.text = text
        if key_text is not None:
            self.key_text = key_text

        key = sum([ord(letter) for letter in self.key_text])

        interval_length = self.max_pritable_char - self.min_printable_char + 1
        shift_size = key % interval_length
        if decryption:
            shift_size = -shift_size

        if shift_size == 0:
            raise ValueError(
                "Unfortunately the encryption key should be changed! The text encryption could not be completed."
            )

        encrypted_msg = ""
        for letter in self.text:
            # keep encrypted char in the [min_printable_char, max_printable_char] = [32, 126]
            ecrypted_letter_code = (
                ord(letter) - self.min_printable_char + shift_size + interval_length
            ) % interval_length + self.min_printable_char
            decrypted_letter = chr(ecrypted_letter_code)
            encrypted_msg += decrypted_letter

        self.encrypted_msg = encrypted_msg
        return encrypted_msg

    def decryption(self, text, key_text):
        return self.encryption(text=text, key_text=key_text, decryption=True)

    def display(self):
        return self.encrypted_msg


if __name__ == "__main__":
    password_for_encryption = input("Type password to encrypt the text: ")
    with open("4.1-4.4.exercises/message.txt", "r", encoding="utf8") as f:
        text = f.read()
    print(f"Encrypting message with Caesar method: {text}")
    caesar = CaesarEncryption(text, password_for_encryption)
    caesar.encryption()

    print(f"Encrypted message: {caesar.display()}")
    with open("4.1-4.4.exercises/message_code.txt", "w", encoding="utf8") as f:
        f.write(caesar.display())

    print("=" * 30, "Decrypt for testing purposes", "=" * 30)
    print(
        f"Decrypted back the message: {caesar.decryption(caesar.display(), password_for_encryption)}"
    )
