from pydantic import BaseModel


class Book(BaseModel):
    title: str
    author: str
    price: float

    def __str__(self) -> str:
        return f"Book {self.title} written by {self.author} costs {self.price:0.2f} Eur"

    def view(self) -> str:
        return str(self)


if __name__ == "__main__":
    book = Book(title="FastAPI", author="Edgaras", price=1_000)
    print(book)
