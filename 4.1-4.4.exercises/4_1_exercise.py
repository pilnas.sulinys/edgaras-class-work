from dataclasses import dataclass #same as pydantic
from math import pi


@dataclass
class Point:
    x: int = 0
    y: int = 0


class Circle:
    def __init__(self, radius: float, *, center: Point) -> None:
        if center is None:
            center = Point()

        self.center = center
        self.radius = radius

    def __repr__(self) -> str:
        return f"Circle(radius={self.radius},center={self.center})"

    def perimeter(self) -> None:
        return 2 * pi * self.radius

    def area(self) -> None:
        return pi * self.radius**2

    def lies_on_circle(self, point: Point) -> bool:
        # Real number comparison requires some degree of accuracy
        return (
            abs(
                (self.center.x - point.x) ** 2
                + (self.center.y - point.y) ** 2
                - self.radius**2
            )
            <= 1e-08
        )


if __name__ == "__main__":
    center = Point(1, 1)
    circle = Circle(1, center=center)
    print(f"Circle area: {circle.area():0.4f}")
    print(f"Circle perimeter: {circle.perimeter():0.4f}")
    test_point = Point(2, 2)

    print(
        f'The point {test_point} {"is" if circle.lies_on_circle(test_point) else "is not"} on the {circle!r}'
    )

    test_point = Point(2, 1)
    print(
        f'The point {test_point} {"is" if circle.lies_on_circle(test_point) else "is not"} on the {circle!r}'
    )
